V’s Barbershop is an authentic, classic barbershop offering men's and boy's haircuts, old fashion straight-edge shaves, and masculine facials.

Address: 1632 N Milwaukee Ave, Chicago, IL 60647, USA

Phone: 773-661-2988

Website: https://vbarbershop.com/locations/chicago-wicker-park-bucktown/
